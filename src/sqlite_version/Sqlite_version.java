/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite_version;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Sérgio Dias (scdias@outlook.com)
 */
public class Sqlite_version {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("sqlite_versionPU");
        EntityManager em = emf.createEntityManager();
        Query query = em.createNativeQuery("SELECT sqlite_version()");
        String resultado = (String) query.getSingleResult();
        em.close();
        emf.close();
        System.out.println(resultado);
    }
    
}
